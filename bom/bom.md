|       | References	  | Value	        | Footprint	                  | Quantity |
|-------|---------------|---------------|-----------------------------|----------|
| 1			| C1	| 1uF	| C_0805_2012Metric	| 1
| 2			| C2	| 0.1uF	| C_0805_2012Metric	| 1
| 3			| R1, R3, R4, R5, R6, R7, R8, R9, R10, R11, R12	| 10k	| R_0805_2012Metric	| 11
| 4			| R2	| 470	| R_0805_2012Metric	| 1
| 5			| D1	| TO-1608BC-MRE	| LED_0603_1608Metric	| 1
| 6			| D2	| TCA8418	| QFN-24-1EP_4x4mm_P0.5mm_EP2.6x2.6mm	| 1
| 7			| SW1, SW2, SW3, SW4, SW5, SW6, SW7, SW8, SW9, SW10, SW11, SW12, SW13	| KLS7-TS6604	| SW_SPST_PTS645	| 13
| 8			| J1, J2	| PBS-10	| PinSocket_1x10_P2.54mm_Vertical	| 2
